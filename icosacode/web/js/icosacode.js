if (Number.prototype.toRadians === undefined) {
  Number.prototype.toRadians = function () {
    return this * Math.PI / 180;
  };
}
if (Number.prototype.toDegrees === undefined) {
  Number.prototype.toDegrees = function () {
    return this * 180 / Math.PI;
  };
}

var red = new THREE.LineBasicMaterial({ color: 0xff0000 });

//Config
//var numRows = 5;
//var charset = "basic25";
var numRows = 4;
var charset = 'novowel16';
var midpoints = Array();
var maxZoomLevel = 20;
var minZoomLevel = 3;

//Global variables
var testPoint;
var scene;
var camera;
var renderer;
var earth = new THREE.Object3D();
var targetLevel = 0;
var selectionMade = false;

//Static data
midpoints[0]  = Array( 50,  36);
midpoints[1]  = Array( 50, 108);
midpoints[2]  = Array( 50, 180);
midpoints[3]  = Array( 50,-108);
midpoints[4]  = Array( 50,- 36);
midpoints[5]  = Array(-10,   0);
midpoints[6]  = Array( 10,  36);
midpoints[7]  = Array(-10,  72);
midpoints[8]  = Array( 10, 108);
midpoints[9]  = Array(-10, 144);
midpoints[10] = Array( 10, 180);
midpoints[11] = Array(-10,-144);
midpoints[12] = Array( 10,-108);
midpoints[13] = Array(-10,- 72);
midpoints[14] = Array( 10,- 36);
midpoints[15] = Array(-50,   0);
midpoints[16] = Array(-50,  72);
midpoints[17] = Array(-50, 144);
midpoints[18] = Array(-50,-144);
midpoints[19] = Array(-50,- 72);
var faceUp = Array();
faceUp[0] = true;
faceUp[1] = true;
faceUp[2] = true;
faceUp[3] = true;
faceUp[4] = true;
faceUp[5] = true;
faceUp[6] = false;
faceUp[7] = true;
faceUp[8] = false;
faceUp[9] = true;
faceUp[10] = false;
faceUp[11] = true;
faceUp[12] = false;
faceUp[13] = true;
faceUp[14] = false;
faceUp[15] = false;
faceUp[16] = false;
faceUp[17] = false;
faceUp[18] = false;
faceUp[19] = false;
function getCharSet(name) {
  if (name == 'basic25') {
    return [['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t'],
            ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t',
             'u','v','w','x','y']];
  } else if (name == 'resilient16') {
    return [['a','b','c','d','e','g','h','j','k','l','m','q','r','s','t','u','v','w','x','y'],
            ['a','c','d','e','g','h','j','k','q','r','t','u','v','w','x','y']];
  } else if (name == 'novowel16') {
    return [['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'],
            ['c','d','g','h','j','k','l','m','q','r','s','t','v','w','x','z']];
  }
}
var isocDegVertices = [
    [ 90,  0],
    [ 30,  0],[ 30,72],[ 30,144],[ 30,-144],[ 30,- 72],
    [-30,-36],[-30,36],[-30,108],[-30, 180],[-30,-108],
    [-90,  0]
];
var isocVertices = Array();
for (var i = 0; i < 12; i++) {
  var c = toCartesian(isocDegVertices[i][0], isocDegVertices[i][1]);
  isocVertices[i] = new Location(c[0], c[1], c[2]);
}
var isocFaces = Array();
isocFaces[0]  = new Div(isocVertices[0], isocVertices[1],  isocVertices[2],  true,  0);
isocFaces[1]  = new Div(isocVertices[0], isocVertices[2],  isocVertices[3],  true,  0);
isocFaces[2]  = new Div(isocVertices[0], isocVertices[3],  isocVertices[4],  true,  0);
isocFaces[3]  = new Div(isocVertices[0], isocVertices[4],  isocVertices[5],  true,  0);
isocFaces[4]  = new Div(isocVertices[0], isocVertices[5],  isocVertices[1],  true,  0);
isocFaces[5]  = new Div(isocVertices[1], isocVertices[6],  isocVertices[7],  true,  0);
isocFaces[6]  = new Div(isocVertices[1], isocVertices[2],  isocVertices[7],  false, 0);
isocFaces[7]  = new Div(isocVertices[2], isocVertices[7],  isocVertices[8],  true,  0);
isocFaces[8]  = new Div(isocVertices[2], isocVertices[3],  isocVertices[8],  false, 0);
isocFaces[9]  = new Div(isocVertices[3], isocVertices[8],  isocVertices[9],  true,  0);
isocFaces[10] = new Div(isocVertices[3], isocVertices[4],  isocVertices[9],  false, 0);
isocFaces[11] = new Div(isocVertices[4], isocVertices[9],  isocVertices[10], true,  0);
isocFaces[12] = new Div(isocVertices[4], isocVertices[5],  isocVertices[10], false, 0);
isocFaces[13] = new Div(isocVertices[5], isocVertices[10], isocVertices[6],  true,  0);
isocFaces[14] = new Div(isocVertices[5], isocVertices[1],  isocVertices[6],  false, 0);
isocFaces[15] = new Div(isocVertices[6], isocVertices[7],  isocVertices[11], false, 0);
isocFaces[16] = new Div(isocVertices[7], isocVertices[8],  isocVertices[11], false, 0);
isocFaces[17] = new Div(isocVertices[8], isocVertices[9],  isocVertices[11], false, 0);
isocFaces[18] = new Div(isocVertices[9], isocVertices[10], isocVertices[11], false, 0);
isocFaces[19] = new Div(isocVertices[10], isocVertices[6], isocVertices[11], false, 0);
var highlightStyle =   new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(0, 0, 255, 0.2)'
  })
});
var gridStyle =   new ol.style.Style({
  stroke: new ol.style.Stroke({
    color: 'blue',
    width: 2
  })
});
var nullStyle = new ol.style.Style({
  fill: new ol.style.Fill({
    color: 'rgba(0, 0, 255, 0.01)'
  })
});

function toCartesian(lat, lon) {
  var φ = lat.toRadians();
  var λ = lon.toRadians();
  return [Math.cos(φ) * Math.cos(λ),
  Math.cos(φ) * Math.sin(λ),
  Math.sin(φ)];
}
function toSpherical(x, y, z) {
  var r = Math.sqrt(x * x + y * y);
  if (r == 0) {
    if (z > 0) return [90,0];
    else if (z < 0) return [-90,0];
    else return [0,0]; // (x,y,z) == (0,0,0)
  } else {
    return [90 - Math.atan2(r, z).toDegrees(), Math.atan2(y, x).toDegrees()];
  }
}
function multCartesian(loc, factor) {
  return new Location(loc.x * factor, loc.y * factor, loc.z * factor, true);
}
function addCartesian2(loc1, loc2) {
  return new Location(loc1.x + loc2.x, loc1.y + loc2.y, loc1.z + loc2.z, true);
}
function addCartesian(loc1, loc2, loc3) {
  return new Location(loc1.x + loc2.x + loc3.x,
                      loc1.y + loc2.y + loc3.y,
                      loc1.z + loc2.z + loc3.z,
                      true);
}
function addSpherical(loc1, loc2) {
  return new Location(loc1.lat + loc2.lat, loc1.lon + loc2.lon);
}
function addLatitude(loc, lat) {
  return new Location(loc.lat + lat, loc.lon);
}
function addLongitude(loc, lon) {
  return new Location(loc.lat, loc.lon + lon);
}
function midpoint2(loc1, loc2) {
  return multCartesian(addCartesian2(loc1, loc2), 0.5);
}
function midpoint(loc1, loc2, loc3) {
  return multCartesian(addCartesian(loc1, loc2, loc3), 1 / 3);
}
function waypoint(loc1, loc2, t) {
  var loc1Effect = multCartesian(loc1, 1 - t);
  var loc2Effect = multCartesian(loc2, t);
  return addCartesian2(loc1Effect, loc2Effect);
}
//coords are interpreted as cartesian x/y/z

function Location(x, y, z) {
  var s = toSpherical(x, y, z);
  this.lat = s[0];
  this.lon = s[1];
  this.x = x;
  this.y = y;
  this.z = z;
}
function setLocation(loc, srcloc) {
  loc.s = srcloc.s;
  loc.lat = srcloc.lat;
  loc.lon = srcloc.lon;
  loc.x = srcloc.x;
  loc.y = srcloc.y;
  loc.z = srcloc.z;
}
function cloneLocation(src) {
  return new Location(src.x, src.y, src.z);
}
function locationFromGPS(lat, lon) {
  var c = toCartesian(lat, lon);
  return extrude(new Location(c[0], c[1], c[2]));
}
function Div(corner1, corner2, corner3, up, level) {
  this.corner1 = typeof corner1 !== 'undefined' ? cloneLocation(corner1) : new Location(0, 0);
  this.corner2 = typeof corner2 !== 'undefined' ? cloneLocation(corner2) : new Location(0, 0);
  this.corner3 = typeof corner3 !== 'undefined' ? cloneLocation(corner3) : new Location(0, 0);
  this.up = typeof up !== 'undefined' ? up : true;
  this.level = typeof level !== 'undefined' ? level : -1;
  this.midpoint = midpoint(this.corner1, this.corner2, this.corner3);
}
function isLocationInFace(location, face) {
  var locx = extrude(location);
  var point = new THREE.Vector3(locx.x, locx.y, locx.z);
  var c1 = new THREE.Vector3(face.corner1.x, face.corner1.y, face.corner1.z);
  var c2 = new THREE.Vector3(face.corner2.x, face.corner2.y, face.corner2.z);
  var c3 = new THREE.Vector3(face.corner3.x, face.corner3.y, face.corner3.z);
  var triangle = new THREE.Triangle(c1, c2, c3);
  var line = new THREE.Line3(point, new THREE.Vector3(0,0,0));
  var plane = triangle.plane();
  if(plane.isIntersectionLine(line)) {
    var intersectPoint = plane.intersectLine(line);
    return triangle.containsPoint(intersectPoint);
  } else {
    return false;
  }
}
function extrude(location) {
  var s = toSpherical(location.x, location.y, location.z);
  var c = toCartesian(s[0], s[1]);
  return new Location(c[0], c[1], c[2]);
}
function extrudeLine(locations, numSections) {
  var outLocations = Array();
  for (var i = 0; i < locations.length - 1; i++) {
    var firstLoc = locations[i];
    var secondLoc = locations[i + 1];
    outLocations[outLocations.length] = extrude(firstLoc);
    for (var j = 0; j < numSections - 1; j++) {
      var midLoc = waypoint(firstLoc, secondLoc, (j + 1) / numSections);
      outLocations[outLocations.length] = extrude(midLoc);
    }
  }
  outLocations[outLocations.length] = extrude(locations[locations.length - 1]);
  return outLocations;
}
function drawPoint(location) {
  var geometry = new THREE.SphereGeometry(0.01, 16, 16);
  var sphere = new THREE.Mesh(geometry, red);
  earth.add(sphere);
  cameraFocus = sphere;
  sphere.translateX(location.y);
  sphere.translateY(location.z);
  sphere.translateZ(location.x);
  return sphere;
}
function drawTestPoint(location) {
  testPoint = drawPoint(location);
}
function drawFace(face, extruded, material) {
  extruded = typeof extruded !== 'undefined' ? extruded : false;
  //material = typeof material !== 'undefined' ? material : cyan;
  material = new THREE.LineBasicMaterial({ color: 0xff0000 });
  var gface = new THREE.Geometry();
  var c1 = face.corner1;
  var c2 = face.corner2;
  var c3 = face.corner3;
  if (extruded) {
    c1 = extrude(c1);
    c2 = extrude(c2);
    c3 = extrude(c3);
  }
  drawLine([c1,c2,c3,c1], material);
}
function drawFaces(faces, extruded) {
  extruded = typeof extruded !== 'undefined' ? extruded : false;
  for (var i = 0; i < faces.length; i++) {
    drawFace(faces[i], extruded);
  }
}
function drawFacesWithSubdivision(faces, extruded) {
  extruded = typeof extruded !== 'undefined' ? extruded : false;
  for (var i = 0; i < faces.length; i++) {
    drawFaces(subdivide(faces[i]), extruded);
  }
}
function drawLine(locations, material) {
  var geom = new THREE.Geometry();
  for (var i = 0; i < locations.length; i++) {
    var l = locations[i];
    geom.vertices.push(new THREE.Vector3(l.y, l.z, l.x));
  }
  var line = new THREE.Line(geom, material);
  earth.add(line);
}
function drawMeridian() {
  var offset = new Location(0.02, 0, 0);
  var lineVertices = [
    addCartesian2(isocFaces[0].corner1, offset),
    addCartesian2(isocFaces[0].corner2, offset),
    addCartesian2(extrude(midpoint2(isocFaces[5].corner2, isocFaces[5].corner3)), offset),
    addCartesian2(isocFaces[19].corner3, offset)
  ];
  var extrudedLine = extrudeLine(lineVertices, 5);
  //var mat = new THREE.LineBasicMaterial({ color: 0xff0000 });
  //mat.linewidth = 2;
  drawLine(extrudedLine, red);
}
function setDiv(dstDiv, srcDiv) {
  dstDiv.midpoint = cloneLocation(srcDiv.midpoint);
  dstDiv.corner1 = cloneLocation(srcDiv.corner1);
  dstDiv.corner2 = cloneLocation(srcDiv.corner2);
  dstDiv.corner3 = cloneLocation(srcDiv.corner3);
  dstDiv.up = srcDiv.up;
  dstDiv.level = srcDiv.level;
}
function cloneDiv(srcDiv) {
  //console.log((new Error()).stack);
  return new Div(srcDiv.corner1, srcDiv.corner2, srcDiv.corner3, srcDiv.up, srcDiv.level);
}
function coordsToDiv(coords) {
  return new Div(new Location(coords[0][0],coords[0][1],coords[0][2]),
                 new Location(coords[1][0], coords[1][1], coords[1][2]),
                 new Location(coords[2][0], coords[2][1], coords[2][2]));
}
function cornerDegreesToDiv(corners) {
  return coordsToDiv(toCartesian());
}
function subdivide(parent) {
  var vertices = Array();
  var faces = Array();
  var divs = Array();
  var divLevel = parent.level + 1;
  //init arrays
  for (var i = 0; i < numRows; i++) {
    vertices[i] = Array();
    faces[i] = Array();
  }
  vertices[numRows] = Array();
  var allDivNum = 0;
  if (parent.up) {
    //first row (0)
    //red
    vertices[0] = [parent.corner1];
    //middle rows
    for (var row = 1; row < numRows; row++) {
      //get orange vertices
      var f = row / numRows;
      var orange1 = waypoint(parent.corner1, parent.corner2, f);
      var orange2 = waypoint(parent.corner1, parent.corner3, f);
      vertices[row] = [orange1];
      for (var col = 1; col < row; col++) {
        var f = col / row;
        //middle yellow vertices
        vertices[row][col] = waypoint(orange1, orange2, f);
      }
      vertices[row][row] = orange2;
      for (var col = 0; col < row; col++) {
        divs[allDivNum] = new Div(vertices[row - 1][col],
                                  vertices[row][col],
                                  vertices[row][col + 1],
                                  true,
                                  divLevel);
        allDivNum++;
        if (col < row - 1) {
          divs[allDivNum] = new Div(vertices[row - 1][col],
                                    vertices[row - 1][col + 1],
                                    vertices[row][col + 1],
                                    false,
                                    divLevel);
          allDivNum++;
        }
      }
    }
    //last row
    var row = numRows;
    //red
    vertices[row] = [
    parent.corner2
    ];
    for (var col = 1; col < numRows; col++) {
      var f = col / numRows;
      //final yellow vertices
      vertices[row][col] = waypoint(parent.corner2, parent.corner3, f);
    }
    //red
    vertices[row][row] = parent.corner3;
    for (var col = 0; col < row; col++) {
      divs[allDivNum] = new Div(vertices[row - 1][col],
                                vertices[row][col],
                                vertices[row][col + 1],
                                true,
                                divLevel);
      allDivNum++;
      if (col < row - 1) {
        divs[allDivNum] = new Div(vertices[row - 1][col],
                                  vertices[row - 1][col + 1],
                                  vertices[row][col + 1],
                                  false,
                                  divLevel);
        allDivNum++;
      }
    }
  } else { //!up
    //first row (0)
    //red
    vertices[0] = [parent.corner1];
    for (var col = 1; col < numRows; col++) {
      var f = col / numRows;
      //yellow
      vertices[0][col] = waypoint(parent.corner1, parent.corner2, f);
    }
    //red
    vertices[0][numRows] = parent.corner2;
    //middle rows vertices
    for (var row = 1; row < numRows; row++) {
      //orange
      var f = row / numRows;
      var orange1 = waypoint(parent.corner1, parent.corner3, f);
      var orange2 = waypoint(parent.corner2, parent.corner3, f);
      vertices[row][0] = orange1;
      for (var col = 1; col < numRows - row; col++) {
        var f = col / (numRows - row);
        //middle yellow vertices
        vertices[row][col] = waypoint(orange1, orange2, f);
      }
      vertices[row][numRows - row] = orange2;
      for (var col = 0; col <= numRows - row; col++) {
        divs[allDivNum] = new Div(vertices[row - 1][col],
                                  vertices[row - 1][col + 1],
                                  vertices[row][col],
                                  false,
                                  divLevel);
        allDivNum++;
        if (col < numRows - row) {
          divs[allDivNum] = new Div(vertices[row - 1][col + 1],
                                    vertices[row][col],
                                    vertices[row][col + 1],
                                    true,
                                    divLevel);
          allDivNum++;
        }
      }
    }
    //last row
    //red
    vertices[numRows] = [parent.corner3];
    divs[allDivNum] = new Div(vertices[numRows - 1][0],
                              vertices[numRows - 1][1],
                              vertices[numRows][0],
                              false,
                              divLevel);
  }
  return divs;
}
function getDist(loc1, loc2) {
  var R = 6371000; // metres
  var φ1 = loc1.lat.toRadians();
  var φ2 = loc2.lat.toRadians();
  var Δφ = (loc2.lat - loc1.lat).toRadians();
  var Δλ = (loc2.lon - loc1.lon).toRadians();
  var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
  Math.cos(φ1) * Math.cos(φ2) *
  Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return R * c;
}
//this can be massively optimised

function getNearestFace(loc, parentFace) {
  var shortestDist = 20000000; //(more than earth diameter)
  var closestFaceNum = 0;
  var closestFace = new Div();
  if (parentFace.level < 0) {
    for (i = 0; i < 20; i++) {
      var thisFace = cloneDiv(isocFaces[i]);
      var thisDist = getDist(thisFace.midpoint, loc);
      if (thisDist < shortestDist) {
        shortestDist = thisDist;
        closestFaceNum = i;
        closestFace = thisFace;
      }
    }
  } else {
    var subdivisions = subdivide(parentFace);
    for (var divNum = 0; divNum < subdivisions.length; divNum++) {
      var thisFace = cloneDiv(subdivisions[divNum]);
      var thisDist = getDist(thisFace.midpoint, loc);
      //console.log(thisDist);
      if (thisDist < shortestDist) {
        shortestDist = thisDist;
        closestFaceNum = divNum;
        closestFace = thisFace;
      }
    }
  }
  setDiv(parentFace, closestFace);
  return closestFaceNum;
}
function getContainingFace(location, parentFace) {
  var containingFace = new Div();
  var containingFaceNum = 0;
  if (parentFace.level < 0) {
    for (i = 0; i < 20; i++) {
      var thisFace = cloneDiv(isocFaces[i]);
      if(isLocationInFace(location, thisFace)) {
        containingFace = thisFace;
        containingFaceNum = i;
        break;
      }
    }
  } else {
    var subdivisions = subdivide(parentFace);
    for (var divNum = 0; divNum < subdivisions.length; divNum++) {
      var thisFace = cloneDiv(subdivisions[divNum]);
      if(isLocationInFace(location, thisFace)) {
        containingFace = thisFace;
        containingFaceNum = divNum;
        break;
      }
    }
  }
  setDiv(parentFace, containingFace);
  return containingFaceNum;
}

function getShortCode(loc, level) {
  level = typeof level !== 'undefined' ? level : 0;
  var parentFace = new Div();
  var face = getContainingFace(loc, parentFace);
  var shortCode = getCharSet(charset)[0][face];
  var thisLevel = 1;
  while (thisLevel <= level) {
    face = getContainingFace(loc, parentFace);
    shortCode += getCharSet(charset)[1][face];
    thisLevel++;
  }
  return shortCode;
}
function getShortCodeWithinMetres(lat, lon, metres) {
  var c = toCartesian(lat, lon);
  var loc = new Location(c[0], c[1], c[2]);
  var parentFace = new Div();
  var faceNum = getContainingFace(loc, parentFace);
  var shortCode = getCharSet(charset) [0][faceNum];
  var thisDist = getDist(parentFace.midpoint, loc);
  var co = 0;
  while (thisDist > metres) {
    faceNum = getContainingFace(loc, parentFace);
    shortCode += getCharSet(charset) [1][faceNum];
    thisDist = getDist(parentFace.midpoint, loc);
    if (co >= 20) return thisDist;
    co++;
  }
  return shortCode;
}
function getCodeFromFace(face) {
  return getShortCode(face.midpoint, face.level);
}
function getFaceContainingLocation(location, level) {
  level = typeof level !== 'undefined' ? level : 0;
  var parentFace = new Div();
  var face = getContainingFace(location, parentFace);
  var thisLevel = 1;
  while (thisLevel <= level) {
    face = getContainingFace(location, parentFace);
    thisLevel++;
  }
  return parentFace;
}
function getFacesContainingLocationUpToLevel(location, level) {
  level = typeof level !== 'undefined' ? level : 0;
  var faces = [];
  var parentFace = new Div();
  getContainingFace(location, parentFace);
  faces[faces.length] = cloneDiv(parentFace);
  var thisLevel = 1;
  while (thisLevel <= level) {
    getContainingFace(location, parentFace);
    faces[faces.length] = cloneDiv(parentFace);
    thisLevel++;
  }
  return faces;
}
function getFacesAtLevel(level, parentFaces=null) {
  let faces = [];
  for (i = 0; i < isocFaces.length; i++) {
    // while (lvl < level) {
    //   ;
    // }
    parentFaces = getFacesAtLevel(level-1);
      for (i = 0; i < parentFaces.length; i++) {
          faces = faces.concat(subdivide(parentFaces[i]));
      }
  }
  return faces;
}
function isFaceWithinRadius(face, location, metres) {
  if(getDist(face.corner1, location) > metres) return false;
  if(getDist(face.corner2, location) > metres) return false;
  if(getDist(face.corner3, location) > metres) return false;
  return true;
}
function isFaceCentreWithinRadius(face, location, metres) {
  if(getDist(face.midpoint, location) > metres) return false;
  return true;
}
function getFaceContainingLocationInsideRadius(location, metres) {
  metres = typeof metres !== 'undefined' ? metres : 100;
  var faces = new Array();
  var parentFace = new Div();
  getContainingFace(location, parentFace);
  faces[faces.length] = cloneDiv(parentFace);
  while (!isFaceWithinRadius(parentFace, location, metres)) {
    getContainingFace(location, parentFace);
    faces[faces.length] = cloneDiv(parentFace);
  }
  return faces[faces.length-1];
}
function getFaceWithCentreWithinRadius(location, metres) {
  metres = typeof metres !== 'undefined' ? metres : 100;
  var faces = new Array();
  var parentFace = new Div();
  getContainingFace(location, parentFace);
  faces[faces.length] = cloneDiv(parentFace);
  while (!isFaceCentreWithinRadius(parentFace, location, metres)) {
    getContainingFace(location, parentFace);
    faces[faces.length] = cloneDiv(parentFace);
  }
  return faces[faces.length-1];
}
function getFaceNumFromChar(char, firstChar) {
  if (firstChar) {
    for (var i = 0; i < getCharSet(charset) [0].length; i++) {
      if (getCharSet(charset)[0][i] == char) return i;
    }
  } else {
    for (var i = 0; i < getCharSet(charset) [1].length; i++) {
      if (getCharSet(charset)[1][i] == char) return i;
    }
  }
}
function getFaceFromCode(code) {
  //console.log("getting face from code:", code);
  var face = new Div();
  if (code.length > 0) {
    var char = code[0];
    var divNum = getFaceNumFromChar(char, true);
    if (code.length == 1) {
      face = cloneDiv(isocFaces[divNum]);
    } else {
      face = getFaceFromCodeCore(code.substr(1), isocFaces[divNum]);
    }
  }
  //console.log("got face:",face);
  return face;
}
function getFaceFromCodeCore(code, parentFace) {
  var face;
  if (code.length > 0) {
    var char = code[0];
    var subdivisions = subdivide(parentFace);
    face = getFaceFromCodeCore(code.substr(1), subdivisions[getFaceNumFromChar(char, false)]);
  } else { //base case
    face = cloneDiv(parentFace);
  }
  return face;
}
function getLocationFromCode(code) {
  return extrude(getFaceFromCode(code).midpoint);
}
function mustEqual(testName, checkValue, refValue) {
  if (checkValue > refValue - 1e-12 &&
    checkValue < refValue + 1e-12) {
    console.log('PASS: ' + testName + '. (' + refValue + ')');
  } else {
    console.log('  FAIL: ' + testName);
    console.log('  needed \'' + refValue + '\' but found \'' + checkValue + '\'.');
  }
}
function checkCoords(checkFace, name, refCorners) {
  var cNames = [
    checkFace.up ? 'top' : 'top left',
    checkFace.up ? 'bottom left' : 'top right',
    checkFace.up ? 'bottom right' : 'bottom'
  ];
  var corners = [
    checkFace.corner1,
    checkFace.corner2,
    checkFace.corner3
  ];
  for (var i = 0; i < 3; i++) {
    mustEqual(name + ' ' + cNames[i] + ' corner lat', corners[i].lat, refCorners[i][0]);
    mustEqual(name + ' ' + cNames[i] + ' corner lon', corners[i].lon, refCorners[i][1]);
  }
}
function getAccuracyForNumChars(numChars) {
  var acc = 20000000;
  var p = getDist(isocFaces[0].corner1, isocFaces[0].midpoint);
  var scale = Math.pow(numRows, numChars - 1);
  return p / scale;
}
function drawLevel(level, extruded) {
  level = typeof level !== 'undefined' ? level : 0;
  extruded = typeof extruded !== 'undefined' ? extruded : false;
  if (level == 0) {
    for (var i = 0; i < 20; i++) {
    drawFace(isocFaces[i], extruded);
    }
  } else {
    for (var i = 0; i < 20; i++) {
      var divs = subdivide(isocFaces[i]);
      drawLevelCore(level, extruded, isocFaces[i]);
    }
  }
}
function drawLevelCore(level, extruded, parent) {
  //drawFace(parent, extruded);
  if (parent.level == level) {
    drawFace(parent, extruded);
  } else {
    var divs = subdivide(parent);
    for (var i = 0; i < divs.length; i++) {
      drawLevelCore(level, extruded, divs[i]);
    }
  }
}

function setupScene() {
  document.getElementsByTagName('body') [0].innerHTML = '';
  scene = new THREE.Scene();
  scene.add(earth);
  camera = new THREE.PerspectiveCamera(50, 
                                       window.innerWidth / window.innerHeight, 
                                       0.1, 
                                       10);
  //var camera = new THREE.OrthographicCamera(window.innerWidth / - 400, 
  //                                          window.innerWidth / 400, 
  //                                          window.innerHeight / 400, 
  //                                          window.innerHeight / - 400, -2, 1000 );
  camera.position.set(0, 1, 3);
  //camera.lookAt(new THREE.Vector3(0, 0, 0));
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);
  var directionalLight = new THREE.DirectionalLight(16777215, 1);
  directionalLight.position.set( - 0.5, 0.5, 1).normalize();
  scene.add(directionalLight);
  var light = new THREE.HemisphereLight(16777215, 4473924, 1);
  scene.add(light);
  var geometry = new THREE.SphereGeometry(0.97, 32, 32);
  var shadow = new THREE.LineBasicMaterial({color: 0});
  shadow.transparent = true;
  shadow.opacity = 0.7;
  var sphere = new THREE.Mesh(geometry, shadow);
  scene.add(sphere);
}

function doRender(animate) {
  animate = typeof animate !== 'undefined' ? animate : false;
  var earthRotateSpeed = 0.00002;
  //var earthRotateSpeed = 0;
  var zoomSpeed = 1.01;
  var render = function () {
    if (animate) {
      requestAnimationFrame(render);
      earth.rotateOnAxis(new THREE.Vector3(0, 1, 0), earthRotateSpeed);
      //camera.lookAt(cameraFocus.geometry.boundingSphere);

      var eye = /*cameraFocus.position.clone();*/new THREE.Vector3(0,0,0);
      cameraFocus.localToWorld(eye);
      //window.console.log(eye);
      camera.lookAt(eye);
      camera.fov /= zoomSpeed;
      testPoint.scale.multiplyScalar(1/zoomSpeed);
      camera.updateProjectionMatrix();
    }
    renderer.render(scene, camera);
  };
  render();
}

// //getShortCode(50, -4, 50);
// //getFaceFromCode("espark");
// setupScene();
// var testLocation = locationFromGPS(50, -6.1);
// getShortCodeWithinMetres(50, 4.50045, 20);
// //getAccuracyForNumChars(8);
// drawLevel(1, true);
// var faces = getFacesContainingLocationUpToLevel(testLocation, 8);
// drawFacesWithSubdivision(faces, true);
// drawFace(isocFaces[0]);
// drawTestPoint(testLocation);
// //drawPoint(getLocationFromCode("ammm"));
// drawMeridian();
// doRender(true);
// //isLocationInFace(testLocation, isocFaces[0]);


//-----------------------------------


function projGPSToMercator(lonlat) {
  return ol.proj.transform([lonlat[0], lonlat[1]], 
                           new ol.proj.Projection({code: "EPSG:4326"}), 
                           new ol.proj.Projection({code: "EPSG:3857"}));
}
function projLocToMercator(loc) {
  return projGPSToMercator([loc.lon, loc.lat]);
}
function projMercatorToGPS(xy) {
  return ol.proj.transform([xy[0], xy[1]],
                           new ol.proj.Projection({code: "EPSG:3857"}),
                           new ol.proj.Projection({code: "EPSG:4326"}));
}
function projMercatorToLoc(xy) {
  var gps = projMercatorToGPS(xy);
  return locationFromGPS(gps[1], gps[0]);
}
function drawGeodesicPoly3(loc1, loc2, loc3, featuresArray) {
  function toVerts(l1, l2) {
    var vs = Array();
    var numSegments = 8;
    for(var i = 0; i < numSegments; i++) {
      var newV = waypoint(l1, l2, i/numSegments);
      //vs.push(projLocToMercator(newV));
      vs.push([newV.lon, sanitisedLat(newV.lat)]);
    }
    return vs;
  }
  var vertices = Array();
  var vertices = (toVerts(loc1,loc2)).concat(toVerts(loc2,loc3), toVerts(loc3,loc1));
  featuresArray.push({
    'type': 'Feature',
    'geometry': {
      'type': 'Polygon',
      'coordinates': [vertices]
    }
  });
}
/*function drawGeodesicFace(face, featuresArray) {
  drawGeodesicPoly3(face.corner1, face.corner2, face.corner3, featuresArray);
}
function drawGeodesicFaces(faces, featuresArray) {
  for(var i = 0; i < faces.length; i++) {
    drawGeodesicPoly3(faces[i].corner1, faces[i].corner2, faces[i].corner3, featuresArray);
  }
}*/
function sanitisedLat(lat) {
  return lat > -90 ? lat : -89.9999999;
}

function sanitisedLocation(loc) {
  return locationFromGPS(sanitisedLat(loc.lat), loc.lon);
}
/*var features = Array();
for(var f = 0; f < 20; f++) {
  if(f == 2 || f == 10 || f == 11 || f == 18) continue;
  drawGeodesicFace(isocFaces[f], features);
  var subs = subdivide(isocFaces[f]);
}*/
var facesSource = new ol.source.Vector(({
  format: new ol.format.GeoJSON(),
  object: {
    'type': 'FeatureCollection',
    'crs': {
      'type': 'name',
      'properties': {
        'name': 'EPSG:4326'
        //'name': 'EPSG:3857'
      }
    },
    'features': []
  }
}));
var gridSource = new ol.source.Vector(({
  format: new ol.format.GeoJSON(),
  object: {
    'type': 'FeatureCollection',
    'crs': {
      'type': 'name',
      'properties': {
        'name': 'EPSG:4326'
        //'name': 'EPSG:3857'
      }
    },
    'features': []
  }
}));
function locToThree(loc) {
  return new THREE.Vector3(loc.x, loc.y, loc.z);
}
function threeToLoc(vector) {
  return new Location(vector.x, vector.y, vector.z);
}
function addGeodesicCircle(face) {
  var vector = locToThree(face.corner1);
  var axis = (new THREE.Vector3(0,0,0)).add(locToThree(face.midpoint));
  var numSegments = 32;
  var angle = (2.7 * Math.PI) / numSegments;

  function toVerts(l1, l2) {
    var vs = Array();
    //var numSegments = 16;
    //for(var i = 0; i < numSegments; i++) {
    //  var newV = waypoint(l1, l2, i/numSegments);
      //vs.push(projLocToMercator(newV));
    //  vs.push(projGPSToMercator([newV.lon, sanitisedLat(newV.lat)]));
    //}
    vs.push(projLocToMercator(l1), projLocToMercator(l2));
    return vs;
  }
  var v = Array();
  v[0] = projLocToMercator(threeToLoc(vector));
  for(var i = 0; i < numSegments; i++) {
    vector.applyAxisAngle(axis, angle);
    v[v.length] = projLocToMercator(threeToLoc(vector));
  }
  //var v = (toVerts(face.corner1,face.corner2)).concat(toVerts(face.corner2,face.corner3), toVerts(face.corner3,face.corner1));
  var feature = new ol.Feature({
    geometry: new ol.geom.Polygon([v])
  });
  facesSource.addFeature(feature);
}
function addGeodesicTriangle(loc1, loc2, loc3, source=facesSource) {
  function toVerts(l1, l2) {
    var vs = Array();
    var numSegments = 16;
    for(var i = 0; i < numSegments; i++) {
      var newV = waypoint(l1, l2, i/numSegments);
      //vs.push(projLocToMercator(newV));
      vs.push(projGPSToMercator([newV.lon, sanitisedLat(newV.lat)]));
    }
    return vs;
  }
  var vertices = (toVerts(loc1,loc2)).concat(toVerts(loc2,loc3), toVerts(loc3,loc1));
  var feature = new ol.Feature({
    geometry: new ol.geom.Polygon([vertices])
  });
  source.addFeature(feature);
}
function addGeodesicPoint(loc) {
  var vertex = projGPSToMercator([loc.lon, sanitisedLat(loc.lat)]);
  var feature = new ol.Feature({
    geometry: new ol.geom.Circle(vertex)
  });
  facesSource.addFeature(feature);
}
function addGeodesicFace(face) {
  addGeodesicTriangle(face.corner1, face.corner2, face.corner3);
}
function addGeodesicFaces(faces, source=facesSource) {
  for(var i = 0; i < faces.length; i++) {
    addGeodesicTriangle(faces[i].corner1, faces[i].corner2, faces[i].corner3, source);
  }
}
var visibleLayer = new ol.layer.Vector({
  source: facesSource,
  style: highlightStyle
});
var gridLayer = new ol.layer.Vector({
  source: gridSource,
  style: gridStyle
});
var mousePositionControl = new ol.control.MousePosition({
  coordinateFormat: ol.coordinate.createStringXY(4),
  projection: 'EPSG:4326',
  undefinedHTML: '&nbsp;'
});
var map = new ol.Map({
  target: 'map',
  layers: [
    new ol.layer.Tile({
      source: new ol.source.OSM()
      /*source: new ol.source.BingMaps({
        key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
        imagerySet: 'Road'
      })*/
    }),
    visibleLayer,
    gridLayer
  ],
  view: new ol.View({
    center: [0, 0],
    zoom: 3,
    numZoomLevels: null,
    minZoomLevel: 8,
    maxZoomLevel: 16
  }),
  controls: ol.control.defaults({
    attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
      collapsible: false
    })
  }).extend([mousePositionControl])
});
var curZoom = map.getView().getZoom();
/*var select = new ol.interaction.Select();
map.addInteraction(select);
var selectedFeatures = select.getFeatures();*/
function onClickAtLoc(loc) {
  /*facesSource.clear();
  var faces = getFacesContainingLocationUpToLevel(loc, targetLevel);
  addGeodesicFaces(faces);
  selectionMade = !selectionMade;*/
  moveTo(loc);
}
function onDoubleClickAtLoc(loc) {
  //setTimeout(function(){
    moveTo(loc);
  //}, 300);
}
var clickedRecently = false;
var doubleClicked = false
map.on('click', function(e) {
  var pixel = map.getEventPixel(e.originalEvent);
  var position = map.getCoordinateFromPixel(pixel);
  var loc = projMercatorToLoc(position);
  if(clickedRecently) { //double clicked
    onDoubleClickAtLoc(loc);
    clickedRecently = false;
    doubleClicked = true;
  } else { //single clicked
    doubleClicked = false;
    clickedRecently = true;
    //wait for double click
    setTimeout(function() {
      if(!doubleClicked) {
        onClickAtLoc(loc);
      }
    }, 200);
    //remove recent click flag once enough time has elapsed
    setTimeout(function(){
      clickedRecently = false;
    }, 200);
  }
});
function getMinScreenExtentMetres() {
  var extent = map.getView().calculateExtent(map.getSize());
  var centre = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
  var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(extent),'EPSG:3857', 'EPSG:4326');
  var topRight = ol.proj.transform(ol.extent.getTopRight(extent),'EPSG:3857', 'EPSG:4326');
  var topCentre = [centre[0], topRight[1]];
  var bottomCentre = [centre[0], bottomLeft[1]];
  var middleLeft = [bottomLeft[0], centre[1]];
  var middleRight = [topRight[0], centre[1]];
  var heightMetres = getDist(locationFromGPS(topCentre[1],    topCentre[0]),
                             locationFromGPS(bottomCentre[1], bottomCentre[0]));
  var widthMetres =  getDist(locationFromGPS(middleLeft[1],   middleLeft[0]),
                             locationFromGPS(middleRight[1],  middleRight[0]));
  return heightMetres < widthMetres ? heightMetres : widthMetres;
}
function display(id, value) {
  document.getElementById(id).value = value;
}
function moveTo(loc) {
  map.getView().setCenter(projLocToMercator(loc));
}
function clearInput() {
  document.getElementById('info').value = "";
}
function appendChar(character) {
  var el = document.getElementById('info');
  var currentValue = el.value;
  var validChars = currentValue.length > 0 ? getCharSet(charset)[1] : getCharSet(charset)[0];
  //console.log(validChars);
  if(validChars.indexOf(character) != -1) {
    //typed character is valid
    el.value = el.value + character;
  }
}
function getInputCode() {
  return document.getElementById('info').value.toLowerCase();
}
function visitInputCode() {
  visitCode(getInputCode());
}
function getBaseLog(base, x) {
  return Math.log(x) / Math.log(base);
}
function getLevelForBoundingMetres(metres) {
  return Math.floor(1.9*getBaseLog(0.25, metres/10000000) + 2);
}
function getZoomForLevel(level) {
  return Math.floor(2+(level*1.9));
}
function zoomToZoomLevel(zoom) {
  map.getView().setZoom(zoom);
}
function zoomToCode(code) {
  var accAdjust = getInputAccuracy();
  var level = code.length - 1 - accAdjust;
  var zoomLevel = getZoomForLevel(level);
  zoomToZoomLevel(zoomLevel);
}
function visitCode(code) {
  //console.log("visiting code:",code);
  var loc = getLocationFromCode(code);
  moveTo(loc);
  zoomToCode(code);
}
var ctrlDown = false;
var ctrlKey = 17, vKey = 86, cKey = 67;
document.getElementById('info').onkeypress = function(e){
  if (!e) e = window.event;
  var keyCode = e.keyCode || e.which;
  var el = document.getElementById('info');
  var code = el.value.toLowerCase();
  var typedChar = String.fromCharCode(keyCode).toLowerCase();
  var newCode = code+typedChar;
  var validChars = this.selectionStart > 0 ? getCharSet(charset)[1] : getCharSet(charset)[0];
  if(ctrlDown) {
    //ctrl pressed
  } else if (keyCode == '13'){
    // Enter pressed
    visitCode(code);
    return false;
  } else if(keyCode == 8 || keyCode == 46) {
    //backspace || delete
    visitCode(code.substring(0, code.length-1));
  } else if ((keyCode >= 37 && keyCode <= 40) || keyCode == 35 || keyCode == 36) {
    //special
  } else if(validChars.indexOf(typedChar) != -1) {
    //typed character is valid
    visitCode(newCode);
  } else {
    //alert("\""+typedChar+"\" is not a valid character here.");
    el.style.backgroundColor = 'black';
    el.style.color = 'white';
    setTimeout(function(){
      var el = document.getElementById('info');
      el.style.backgroundColor = 'white';
      el.style.color = 'black';
    }, 200);
    return false;
  }
}
document.getElementById('info').onkeydown = function(e){
  if (e.keyCode == ctrlKey) ctrlDown = true;
}
document.getElementById('info').onkeyup = function(e){
  if (e.keyCode == ctrlKey) ctrlDown = false;
}
document.getElementById('address').onkeypress = function(e){
  if (!e) e = window.event;
  var keyCode = e.keyCode || e.which;
  var el = document.getElementById('address');
  var code = el.value.toLowerCase();
  var typedChar = String.fromCharCode(keyCode);
  var newCode = code+typedChar;
  var validChars = this.selectionStart > 0 ? getCharSet(charset)[1] : getCharSet(charset)[0];
  if (keyCode == '13'){
    // Enter pressed
    searchAddress(code);
    return false;
  }
}
function onAccuracyChanged() {
  onMoveEnd();
}
function getInputAccuracy() {
  var accElem = document.getElementById('accuracy');
  return accElem.options[accElem.selectedIndex].value;
}
function onMoveEnd() {
  var extent = map.getView().calculateExtent(map.getSize());
  var centreGPS = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
  var centre = locationFromGPS(centreGPS[1], centreGPS[0]);
  //if(!selectionMade) {
  var face;
  //console.log("zoom",map.getView().getZoom(),"metres:",getMinScreenExtentMetres());
  //if(typeof zoom == 'undefined') {
  //  zoomToCode(getInputCode());
  //  //alert(map.getView().getZoom());
  //}
  var newZoom = map.getView().getZoom();
  if (newZoom > 2) {
    var accAdjust = getInputAccuracy();
    var radiusFactor = Math.pow(4, -accAdjust);
    var radius = getMinScreenExtentMetres()*0.333*radiusFactor;
    face = getFaceContainingLocationInsideRadius(centre, radius);
    //face = getFaceWithCentreWithinRadius(centre, radius);
  } else {
    face = getFaceContainingLocation(centre);
  }
  targetLevel = face.level;
  facesSource.clear();
  gridSource.clear();
  //addGeodesicFace(face);
  //addGeodesicFaces(getFacesAtLevel(2), gridSource);
  addGeodesicCircle(face);
  addGeodesicPoint(face.midpoint);
  //}
  var code = getCodeFromFace(face);
  display('info', code);
  //if(history.pushState) {
  //  history.pushState(null, null, '#'+code);
  //}
  //else {
  location.hash = '#'+code;
  //}
  if (curZoom != newZoom) {
    curZoom = newZoom;
    if (newZoom > maxZoomLevel) zoomToZoomLevel(maxZoomLevel);
    if (newZoom < minZoomLevel) zoomToZoomLevel(minZoomLevel);
  }

}
/*function getJSONP(url, success) {
    var ud = '_' + +new Date,
        script = document.createElement('script'),
        head = document.getElementsByTagName('head')[0]
               || document.documentElement;
    window[ud] = function(data) {
        head.removeChild(script);
        success && success(data);
    };
    script.src = url.replace('callback=?', 'callback=' + ud);
    head.appendChild(script);
}*/
function searchAddress(address) {
  $.getJSON('https://nominatim.openstreetmap.org/search?q='+address+'&format=json', function(data){
    if(data.length > 0) {
      var res = data[0];
      console.log("res",res);
      var lat = parseFloat(res.lat);
      var lon = parseFloat(res.lon);
      var loc = locationFromGPS(lat, lon);
      var bound = res.boundingbox;
      var bottomLeft = locationFromGPS(parseFloat(bound[0]), parseFloat(bound[2]));
      var topRight = locationFromGPS(parseFloat(bound[1]), parseFloat(bound[3]));
      var dist = getDist(bottomLeft, topRight);
      if(dist > 0) {
        var level = getLevelForBoundingMetres(dist);
        zoomToZoomLevel(level);
      }
      moveTo(loc);
      onMoveEnd();
    }
  });
}

var qcode = window.location.hash.substring(1);
clearInput();
for(var i = 0; i < qcode.length; i++) {
  if(i-1 < qcode.length) {
    appendChar(qcode.substring(i,i+1));
  } else {
    appendChar(qcode.substring(i));
  }
}
if(getInputCode() == "") {
  visitCode("gq");  //Default to UK
} else {
  visitInputCode();
}

map.on('moveend', onMoveEnd);
onMoveEnd();
if(qcode.length < 1) {
  setTimeout(function() { clearInput(); location.hash = ""; }, 200);
}
